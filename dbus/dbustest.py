#!/usr/bin/env python

import dbus
import os
def connect():
    if 'PULSE_DBUS_SERVER' in os.environ:
        address = os.environ['PULSE_DBUS_SERVER']
        print "$PULSE_DBUS_SERVER set: " + address
    else:
        bus = dbus.SessionBus()
        server_lookup = bus.get_object("org.PulseAudio1", "/org/pulseaudio/server_lookup1")
        address = server_lookup.Get("org.PulseAudio.ServerLookup1", "Address", dbus_interface="org.freedesktop.DBus.Properties")
        print "$PULSE_DBUS_SERVER was not set: " + address

    return dbus.connection.Connection(address)

conn = connect()
core = conn.get_object(object_path="/org/pulseaudio/core1")

print "Successfully connected to " + core.Get("org.PulseAudio.Core1", "Name", dbus_interface="org.freedesktop.DBus.Properties")

sinks = core.Get("org.PulseAudio.Core1", "Sinks", dbus_interface="org.freedesktop.DBus.Properties")
print len(sinks)
for sinkName in sinks:
    sink = conn.get_object(object_path=sinkName)
    #print sink.Introspect()
    iface = dbus.Interface(sink, 'org.freedesktop.DBus.Properties')
    muteState = iface.Get('org.PulseAudio.Core1.Device', 'Mute', dbus_interface="org.freedesktop.DBus.Properties")
    if muteState:
        iface.Set('org.PulseAudio.Core1.Device', 'Mute', False)
    else:
        iface.Set('org.PulseAudio.Core1.Device', 'Mute', True)


